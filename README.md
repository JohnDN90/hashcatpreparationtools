**Hashcat Preparation Tools**

Useful tools to help separate handshakes from packet-capture files into seperate files and prepare each handshake for use with hashcat.

*DISCLAIMER: These tools are intended for use with auditing the security of wireless networks for which you have AUTHORIZATION. This means you may either use it on wireless systems which you own or wireless systems which you obtain permission for from the network owner. The use of these tools to help "hack your neighbors WiFi" is not encouraged or allowed.*
---

## Why does this exist?

(While reading this, when I refer to "good", "workable', and "bad", I am referring to Pyrit's classification of the quality of the handhsake.)

I created this repository because I noticed a few issues when running hashcat 4.0.1-79-g7f087d0 on my R9 290 GPU.  If more than one handshake was included in the .hccapx file, my cracking speed slowed down.  If I performed a capture on my home network over 72 hours, with multiple devices connecting/disconnecting, I ended up with over 150 handshakes in my capture file and in the converted .hccapx file.  This was enough to slow my cracking speed down from about 116 kH/s to only 14 kH/s (note, all the handshakes were for the same ESSID).  Therefore, I wanted a way to separate the different handshakes from my capture file and only use the single "highest quality" handshake in my .hccapx file so I could obtain maximum speed.

The second issue I noticed was that my capture file with over 150 handshakes included good, workable, and bad handshakes. I wonder if this is what was causing the slow down, perhaps the bad handshakes which were also included in the .hccapx file were generating incorrect hashes and thus hashcat was trying to crack multiple hashes instead of only one.  Regardless, I was unsure of how hashcat handles multiple handshakes, especially mixed quality ones, so I wanted a way to strip my capture file down to a single handshake so I knew exactly what hashcat was working with.

The functionality may already exist somewhere, but after searching the web for a couple days I was unable to find a way to split a capture file into individual handshakes.  Pyrit could separate by ESSID/BSSID but not individual handshakes, the online hccapxsplitter tool could split the .hccapx file into separate handshakes but then I was unable to find a tool which could detect the quality of a handshake in a .hccapx file.  I tried looking at the message_pair in the hexidecimal output from hccapx, per this website https://hashcat.net/wiki/doku.php?id=hccapx, but I noticed that a file with a single "bad" handshake had a 00 in the message_pair location and a different file with a single "good" handshake also had a 00 in the message_pair location so that wasn't giving me enough information to classify the handshake contained in the .hccapx file.

Another feature I desired was the ability to automatically output each ESSID in a capture files to a separate .hccapx file. Furthermore, I thought it'd be nice if the entire post-processing of the capture file (stripping, extracting of handshakes) and conversion to a separate .hccapx for each ESSID was completely automated.

Thus, I created this patch and script to fullfil this need that I had. I ran across several threads in different forums with people searching for similar functionality so I thought I would create a repository and share this with anyone else who may find it useful.

---

## Functionality

This repository provides its functionality in two forms.  First, it patches the pyrit_cli.py file on your system to provide three new commands in Pyrit.  Like the original commands, they can be viewed with a short description by running "pyrit" from the terminal.  The three new commands are:

1. strip_best_handshakes
2. separate_handshakes
3. separate_best_handshakes

strip_best_handshakes:  Parse one or more packet-capture files given by the option -r, extract only packets that are necessary for EAPOL-handshake detection for the highest quality handshake of each AP and write a new dump to the filename given by the option -o. The options -e and -b can be used to filter certain Access-Points. This command includes all handshakes ("good", "workable", and "bad") by default, but this behavior can be changed by editing the lowest_desired_quality varaible in this function.

separate_handshakes: Parse one or more packet-capture files given by the option -r, extract only packets that are necessary for EAPOL-handshake detection of each AP and write a new separate dump for each handshake. The options -e  and -b can be used to filter certain Access-Points. This command outputs all handshakes (good, workable, and bad), regardless of quality.

separate_best_handshakes: Parse one or more packet-capture files given by the option -r, extract only packets that are necessary for EAPOL-handshake detection for only the best handshake of each AP and write a new separate dump for each handshake. The options -e  and -b can be used to filter certain Access-Points. This command includes all handshakes ("good", "workable", and "bad") by default, but this behavior can be changed by editing the lowest_desired_quality varaible in this function.

*WARNING:  These commands do NOT preserve the accuracy of the spread information in the output file. Spread will show as '1' in the output file but this is not accruate!*

The second form of the funtionality of this script comes in the prepareCaptureForHashcat bash script.  This script takes a capture file (i.e. from airodump-ng) as input, strips only the best handshake for each AP to a separate output file, and then creates a separate .hccapx file for each ESSID.  This script utilizes the patched pyrit_cli.py file.

Note: The "highest quality" handshake is defined as the handshake with the highest rating according to Pyrit (good > workable > bad) and with the lowest spread.

---

## Dependencies

The full functionality provided by the scripts and patches in this repository require the following software:

1. pyrit (required, tested on version 0.5.1, commit 692fbe95095baf9689fb923f96377626d536c6d6)
2. cap2hccapx (required, part of hashcat-utils, tested on version 1.8)
3. tshark (optional, for optional commented out functionality in "prepareCaptureForHashcat", tested on version 2.2.6)
4. hashcat (optional, not required for the files in this repository, but needed for cracking, tested on version 4.0.1-79-g7f087d0)

Each of the above software will have its own dependencies which must be satisfied. See the official website and/or documentation for each of the above packages to install them.

---


## Installation

The installation of this software is very simple. First, ensure you have all required dependencies (listed above) installed.  Next, patch the pyrit_cli.py file.  This file is usually installed in /usr/lib/python2.7/dist-packages or /usr/local/lib/python2.7/dist-packages.  The patching can be done using:

patch /path/to/original/pyrit_cli.py /path/to/pyrit_cli_newFeature.patch

I've included an already patched file called pyrit_cli_modified.py. This is included only for reference and is not used in the installation.

Once pyrit has been patched with the added functionality, simply copy the prepareCaptureForHashcat file to the destination of your choice on your system. I copied mine to /usr/bin/prepareCaptureForHashcat so that it was on the PATH.  If you add it to a location not on the PATH by default, be sure to add that location to your PATH environment variable in your $HOME/.bashrc file.

IMPORTANT: It may by necessary to give prepareCaptureForHashcat executable permission by running chmod +x prepareCaptureForHashcat.

Of course the dependencies should be on your PATH as well.  After this, you should be able to use the functionality.

---

## Examples

You can run some basic examples by opening a terminal, cd-ing into the hashcatprepartiontools/example directory and running one of the the four included examples.  These examples are bash scripts and you made need to give them executable permissions as well.