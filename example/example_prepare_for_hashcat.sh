#!/usr/bin/env bash

# This example assumes hashcat and prepareCaptureForHashcat are on your PATH.

# Strip capture file so only the single highest quality handshake remains for
# each AP (using modified Pyrit) and then export each ESSID to a seperate
# .hccapx file using cap2hccapx
prepareCaptureForHashcat wpa2psk-linksys.dump

# Use hashcat with the example dictionary to bruteforce attack our example
# problem and recovery the password. Important: For beginners, you do not
# normally use --potfile-disable, it is only used here so that examples can
# be run multiple times.
hashcat -m 2500 -o example_hashcat_output.txt --potfile-disable handshake_linksys.hccapx dict