#!/usr/bin/env bash

# This example assumes the modified Pyrit is on your PATH.

# Write the best handshake for each ESSID in wpa2psk-linksys.cap to a
# separate file
pyrit -r wpa2psk-linksys.cap separate_best_handshakes


# Note, we only get one handshake output from this because in this
# example, wpa2psk-linksys.cap only has one ESSID

# Dictionary attack using Pyrit on each separate handshake
pyrit -r  linksys_good_spread1_bestHandshake.cap -i dict attack_passthrough
