#!/usr/bin/env bash

# This example assumes the modified pyrit is on your PATH.

# Use the modified pyrit to strip the capture file to only the packets
# required for the highest quality handshake for each AP
pyrit -r wpa2psk-linksys.cap -o wpa2psk-linksys_stripedBest.cap strip_best_handshakes

# Use pyrit with a dictionary attack to solve our example problem.
pyrit -r wpa2psk-linksys_stripedBest.cap -i dict attack_passthrough